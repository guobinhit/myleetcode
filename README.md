# myleetcode 

![author](https://img.shields.io/badge/author-chariesgavin-blueviolet.svg)![language](https://img.shields.io/badge/language-Java%20&%20Python%20&%20Go-blue.svg)![license](https://img.shields.io/badge/license-MIT-brightgreen.svg)


我们的 LeetCode 题解！

## 贡献

欢迎大家贡献代码！

如果你发现一个问题并且想修复它，那么最好的方法就是提交一个 PR，具体请参考「[CONTRIBUTING.md](../master/CONTRIBUTING.md)」文件查看工作流。

当然，你也可以直接在「[Issues](https://gitee.com/guobinhit/myleetcode/issues)」中提问和讨论解题思路。

## 索引

- **学习**
  - [数组 & 字符串](#数组--字符串)
  - [队列 & 栈](#队列--栈)
  - [链表](#链表)
  - [哈希表](#哈希表)
  - [二分查找](#二分查找)
  - [二叉树](#二叉树)
  - [二叉搜索树](#二叉搜索树)
  - [N叉树](#n叉树)
  - [前缀树](#前缀树)
  - [决策树](#决策树)
  - [递归 I](#递归-i)
  - [递归 II](#递归-ii)
  - [并发](#并发)
  - [脚本](#脚本)
  - [数据库](#数据库)
  - [排序算法](#排序算法)
  - [搜索算法](#搜索算法)
- **面试**
  - [常见面试题](#常见面试题)
    - [简单问题](#简单问题)
    - [中等问题](#中等问题)
    - [困难问题](#困难问题)
  - [字节跳动](#字节跳动)
  - [腾讯](#腾讯)
  - [猿题库](#猿题库)
  - [搜狗](#搜狗)
  - [美团点评](#美团点评)
  - [跟谁学](#跟谁学)




## 学习
### 数组 & 字符串

|  #  |      题名     |   题解  |   难度  | 标签                   
|-----|----------------|:---------------:|:--------:|:-------------:
|14|[最长公共前缀](https://leetcode-cn.com/problems/longest-common-prefix/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_14.java) & [Go](../master/codes/go/leetcodes/basmath/learn/array_and_string/14.go)|简单| 字符串简介
|26|[删除排序数组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_26.java) & [Go](../master/codes/go/leetcodes/basmath/learn/array_and_string/26.go)|简单| 总结
|27|[移除元素](https://leetcode-cn.com/problems/implement-strstr/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_27.java) & Python|简单| 双指针技巧
|28|[实现 strStr()](https://leetcode-cn.com/problems/implement-strstr/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_28.java) & Python|简单| 字符串简介
|54|[螺旋矩阵](https://leetcode-cn.com/problems/spiral-matrix/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_54.java) & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basmath/learn/array_and_string/_54.py)|中等| 二维数组简介
|66|[加一](https://leetcode-cn.com/problems/plus-one/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_66.java) & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basmath/learn/array_and_string/_66.py) & [Go](../master/codes/go/leetcodes/basmath/learn/array_and_string/66.go)|简单| 数组简介
|67|[二进制求和](https://leetcode-cn.com/problems/add-binary/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_67.java) & Python|简单| 字符串简介
|118|[帕斯卡三角形](https://leetcode-cn.com/problems/pascals-triangle/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_118.java) & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basmath/learn/array_and_string/_118.py)| 简单| 二维数组简介
|119|[帕斯卡三角形 II](https://leetcode-cn.com/problems/pascals-triangle-ii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_119.java) & Python| 简单| 总结
|151|[翻转字符串里的单词](https://leetcode-cn.com/problems/reverse-words-in-a-string/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_151.java) & Python| 中等| 总结
|167|[两数之和 II - 输入有序数组](https://leetcode-cn.com/problems/two-sum-ii-input-array-is-sorted/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_167.java) & Python| 简单| 双指针技巧
|189|[旋转数组](https://leetcode-cn.com/problems/rotate-array/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_189.java) & Python| 简单| 总结
|209|[长度最小的子数组](https://leetcode-cn.com/problems/two-sum-ii-input-array-is-sorted/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_209.java) & Python| 中等| 双指针技巧
|283|[移动零](https://leetcode-cn.com/problems/move-zeroes/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_283.java) & Python| 简单| 总结
|344|[反转字符串](https://leetcode-cn.com/problems/reverse-string/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_344.java) & Python| 简单| 双指针技巧
|485|[最大连续1的个数](https://leetcode-cn.com/problems/diagonal-traverse/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_485.java) & Python |简单| 双指针技巧
|498|[对角线遍历](https://leetcode-cn.com/problems/diagonal-traverse/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_498.java) & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basmath/learn/array_and_string/_498.py) |中等| 二维数组简介
|557|[反转字符串中的单词 III](https://leetcode-cn.com/problems/reverse-words-in-a-string-iii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_557.java) & Python |简单| 总结
|561|[数组拆分 I](https://leetcode-cn.com/problems/array-partition-i/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_561.java)  & Python|简单| 双指针技巧
|724|[寻找数组的中心索引](https://leetcode-cn.com/problems/find-pivot-index/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_724.java)  & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basmath/learn/array_and_string/_724.py)| 简单 | 数组简介
|747|[至少是其他数字两倍的最大数](https://leetcode-cn.com/problems/largest-number-at-least-twice-of-others/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_747.java)  & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basmath/learn/array_and_string/_747.py)| 简单| 数组简介



### 队列 & 栈

|  #  |      题名     |   题解   |   难度  | 标签                   
|-----|----------------|:---------------:|:--------:|:-------------:
|20|[有效的括号](https://leetcode-cn.com/problems/valid-parentheses/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/queue_stack/_20.java) & Python|简单| Stack: Last-in-first-out Data Structure
|133|[Clone Graph](https://leetcode-cn.com/problems/clone-graph/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/queue_stack/_133.java) & Python|中等| Stack and DFS
|150|[逆波兰表达式求值](https://leetcode-cn.com/problems/evaluate-reverse-polish-notation/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/queue_stack/_150.java) & Python|中等| Stack: Last-in-first-out Data Structure
|155|[最小栈](https://leetcode-cn.com/problems/min-stack/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/queue_stack/_155.java) & Python|简单| Stack: Last-in-first-out Data Structure
|200|[岛屿数量](https://leetcode-cn.com/problems/number-of-islands/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/queue_stack/_200.java) & Python|中等| Queue and BFS
|225|[Implement Stack using Queues](https://leetcode-cn.com/problems/implement-stack-using-queues/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/queue_stack/_225.java) & Python|简单| 总结
|232|[Implement Queue using Stacks](https://leetcode-cn.com/problems/implement-queue-using-stacks/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/queue_stack/_232.java) & Python|简单| 总结
|279|[完全平方数](https://leetcode-cn.com/problems/perfect-squares/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/queue_stack/_279.java) & Python|中等| Queue and BFS
|394|[Decode String](https://leetcode-cn.com/problems/decode-string/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/queue_stack/_394.java) & Python|中等| 总结
|494|[Target Sum](https://leetcode-cn.com/problems/target-sum/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/queue_stack/_494.java) & Python|中等| Stack and DFS
|542|[01 Matrix](https://leetcode-cn.com/problems/01-matrix/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/queue_stack/_542.java) & Python|中等| 总结
|622|[Design Circular Queue](https://leetcode-cn.com/problems/longest-common-prefix/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/queue_stack/_622.java) & Python|中等| Queue: First-in-first-out Data Structure
|733|[Flood Fill](https://leetcode-cn.com/problems/flood-fill/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/queue_stack/_733.java) & Python|简单| 总结
|739|[Daily Temperatures](https://leetcode-cn.com/problems/daily-temperatures/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/queue_stack/_739.java) & Python|中等| Stack: Last-in-first-out Data Structure
|752|[Open the Lock](https://leetcode-cn.com/problems/open-the-lock/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/queue_stack/_752.java) & Python|中等| Queue and BFS
|841|[Keys and Rooms](https://leetcode-cn.com/problems/keys-and-rooms/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/queue_stack/_841.java) & Python|中等| 总结



### 链表

|  #  |      题名     |   题解  |   难度  | 标签                   
|-----|----------------|:---------------:|:--------:|:-------------:
|2|[两数相加](https://leetcode-cn.com/problems/add-two-numbers/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_2.java) & Python|中等| 总结
|19|[删除链表的倒数第N个节点](https://leetcode-cn.com/problems/remove-nth-node-from-end-of-list/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_19.java) & [Go](../master/codes/go/leetcodes/basmath/learn/linked_list/19.go)|中等| Two Pointer Technique
|21|[合并两个有序链表](https://leetcode-cn.com/problems/merge-two-sorted-lists/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_21.java) & [Go](../master/codes/go/leetcodes/basmath/learn/linked_list/21.go)|简单| 总结
|61|[旋转链表](https://leetcode-cn.com/problems/rotate-list/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_61.java) & Python|中等| 总结
|138|[复制带随机指针的链表](https://leetcode-cn.com/problems/copy-list-with-random-pointer/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_138.java) & Python|中等| 总结
|141|[环形链表](https://leetcode-cn.com/problems/linked-list-cycle/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_141.java) & Python|简单| Two Pointer Technique
|142|[环形链表 II](https://leetcode-cn.com/problems/linked-list-cycle-ii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_142.java) & Python|中等| Two Pointer Technique
|160|[相交链表](https://leetcode-cn.com/problems/intersection-of-two-linked-lists/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_160.java) & Python|简单| Two Pointer Technique
|203|[移除链表元素](https://leetcode-cn.com/problems/remove-linked-list-elements/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_203.java) & Python|简单| Classic Problems
|206|[反转链表](https://leetcode-cn.com/problems/reverse-linked-list/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_206.java) & Python|简单| Classic Problems
|234|[回文链表](https://leetcode-cn.com/problems/palindrome-linked-list/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_234.java) & Python|简单| Classic Problems
|328|[奇偶链表](https://leetcode-cn.com/problems/odd-even-linked-list/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_328.java) & Python|中等| Classic Problems
|430|[Flatten a Multilevel Doubly 链表](https://leetcode-cn.com/problems/flatten-a-multilevel-doubly-linked-list/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_430.java) & Python|中等| 总结
|707|[Design 链表](https://leetcode-cn.com/problems/number-of-islands/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_707.java) & Python|简单| Singly 链表



### 哈希表

|  #  |      题名     |   题解    |   难度  | 标签                   
|-----|----------------|:---------------:|:--------:|:-------------:
|1|[两数之和](https://leetcode-cn.com/problems/two-sum/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_1.java) & Python |简单| Practical Application - Hash Map
|3|[无重复字符的最长子串](https://leetcode-cn.com/problems/longest-substring-without-repeating-characters/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_3.java) & Python |中等| 总结
|36|[有效的数独](https://leetcode-cn.com/problems/valid-sudoku/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_36.java) & Python |中等| Practical Application - Design the Key
|49|[字母异位词分组](https://leetcode-cn.com/problems/group-anagrams/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_49.java) & Python |中等| Practical Application - Design the Key
|136|[只出现一次的数字](https://leetcode-cn.com/problems/single-number/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_136.java) & Python |简单| Practical Application - Hash Set
|202|[快乐数](https://leetcode-cn.com/problems/happy-number/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_202.java) & Python |简单| Practical Application - Hash Set
|205|[Isomorphic Strings](https://leetcode-cn.com/problems/isomorphic-strings/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_205.java) & Python |简单| Practical Application - Hash Map
|217|[存在重复元素](https://leetcode-cn.com/problems/contains-duplicate/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_217.java) & Python |简单| Practical Application - Hash Set
|219|[存在重复元素 II](https://leetcode-cn.com/problems/contains-duplicate-ii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_219.java) & Python |简单| Practical Application - Hash Map
|347|[前K个高频元素](https://leetcode-cn.com/problems/top-k-frequent-elements/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_347.java) & Python |中等| 总结
|349|[Intersection of Two Arrays](https://leetcode-cn.com/problems/intersection-of-two-arrays/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_349.java) & Python |简单| Practical Application - Hash Set
|350|[两个数组的交集 II](https://leetcode-cn.com/problems/intersection-of-two-arrays-ii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_350.java) & Python |简单| Practical Application - Hash Map
|380|[常数时间插入、删除和获取随机元素](https://leetcode-cn.com/problems/insert-delete-getrandom-o1/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_380.java) & Python |中等| 总结
|387|[字符串中的第一个唯一字符](https://leetcode-cn.com/problems/first-unique-character-in-a-string/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_387.java) & Python |简单| Practical Application - Hash Map
|454|[4Sum II](https://leetcode-cn.com/problems/4sum-ii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_454.java) & Python |中等| 总结
|599|[Minimum Index Sum of Two Lists](https://leetcode-cn.com/problems/minimum-index-sum-of-two-lists/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_599.java) & Python |简单| Practical Application - Hash Map
|652|[Find Duplicate Subtrees](https://leetcode-cn.com/problems/find-duplicate-subtrees/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_652.java) & Python |中等| Practical Application - Design the Key
|705|[Design HashSet](https://leetcode-cn.com/problems/design-hashset/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_705.java) & Python |简单| Design a Hash Table
|706|[Design HashMap](https://leetcode-cn.com/problems/design-hashmap/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_706.java) & Python |简单| Design a Hash Table
|771|[Jewels and Stones](https://leetcode-cn.com/problems/jewels-and-stones/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_771.java) & Python |简单| 总结


### 二分查找

|  #  |      题名     |   题解    |   难度  | 标签                   
|-----|----------------|:---------------:|:--------:|:-------------:
|4|[寻找两个有序数组的中位数](https://leetcode-cn.com/problems/median-of-two-sorted-arrays/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_4.java) & Python |困难| More Practices II
|33|[搜索旋转排序数组](https://leetcode-cn.com/problems/search-in-rotated-sorted-array/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_33.java) & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basmath/learn/binary_search/_33.py)|中等| Template I
|34|[在排序数组中查找元素的第一个和最后一个位置](https://leetcode-cn.com/problems/find-first-and-last-position-of-element-in-sorted-array/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_34.java) & Python |中等| Template III
|50|[Pow(x, n)](https://leetcode-cn.com/problems/powx-n/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_50.java) & Python |中等| 总结
|69|[x的平方根](https://leetcode-cn.com/problems/sqrtx/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_69.java) & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basmath/learn/binary_search/_69.py)|简单| Template I
|153|[Find Minimum in Rotated Sorted Array](https://leetcode-cn.com/problems/find-minimum-in-rotated-sorted-array/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_153.java) & Python|中等| Template II
|154|[Find Minimum in Rotated Sorted Array II](https://leetcode-cn.com/problems/find-minimum-in-rotated-sorted-array-ii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_154.java) & Python| 困难| More Practices
|162|[寻找峰值](https://leetcode-cn.com/problems/find-peak-element/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_162.java) & Python|中等| Template II
|167|[两数之和 II - 输入有序数组](https://leetcode-cn.com/problems/two-sum-ii-input-array-is-sorted/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_167.java) & Python |简单| More Practices
|278|[第一个错误的版本](https://leetcode-cn.com/problems/first-bad-version/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_278.java) & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basmath/learn/binary_search/_278.py)|简单| Template II
|287|[Find the Duplicate Number](https://leetcode-cn.com/problems/find-the-duplicate-number/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_287.java) & Python |中等| More Practices II
|349|[Intersection of Two Arrays](https://leetcode-cn.com/problems/intersection-of-two-arrays/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_349.java) & Python |简单| More Practices
|350|[两个数组的交集 II](https://leetcode-cn.com/problems/intersection-of-two-arrays-ii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_350.java) & Python |简单| More Practices
|367|[Valid Perfect Square](https://leetcode-cn.com/problems/valid-perfect-square/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_367.java) & Python |简单| 总结
|374|[Guess Number Higher or Lower](https://leetcode-cn.com/problems/guess-number-higher-or-lower/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_374.java) & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basmath/learn/binary_search/_374.py)|简单| Template I
|410|[Split Array Largest Sum](https://leetcode-cn.com/problems/split-array-largest-sum/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_410.java) & Python |困难| More Practices II
|658|[Find K Closest Elements](https://leetcode-cn.com/problems/find-k-closest-elements/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_658.java) & Python |中等| Template III
|704|[Binary Search](https://leetcode-cn.com/problems/binary-search/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_704.java) & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basmath/learn/binary_search/_704.py)|简单| Background
|719|[Find K-th Smallest Pair Distance](https://leetcode-cn.com/problems/find-k-th-smallest-pair-distance/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_719.java) & Python |困难| More Practices II
|744|[Find Smallest Letter Greater Than Target](https://leetcode-cn.com/problems/find-smallest-letter-greater-than-target/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_744.java) & Python|简单| 总结



### 二叉树

|  #  |      题名     |   题解    |   难度  | 标签                   
|-----|----------------|:---------------:|:--------:|:-------------:
|94|[二叉树的中序遍历](https://leetcode-cn.com/problems/binary-tree-inorder-traversal/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_94.java) & Python |中等| 树的遍历
|101|[对称二叉树](https://leetcode-cn.com/problems/symmetric-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_101.java) & Python |简单| 运用递归解决树问题
|102|[二叉树的层次遍历](https://leetcode-cn.com/problems/binary-tree-level-order-traversal/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_102.java) & Python |中等| 树的遍历
|104|[二叉树的最大深度](https://leetcode-cn.com/problems/maximum-depth-of-binary-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_104.java) & Python |简单| 运用递归解决树问题
|105|[从前序与中序遍历序列构造二叉树](https://leetcode-cn.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_105.java) & Python |中等| 总结
|106|[Construct Binary Tree from Inorder and Postorder Traversal](https://leetcode-cn.com/problems/construct-binary-tree-from-inorder-and-postorder-traversal/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_106.java) & Python |中等| 总结
|112|[Path Sum](https://leetcode-cn.com/problems/path-sum/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_112.java) & Python |简单| 运用递归解决树问题
|116|[填充每个节点的下一个右侧节点指针](https://leetcode-cn.com/problems/populating-next-right-pointers-in-each-node/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_116.java) & Python |中等| 总结
|117|[Populating Next Right Pointers in Each Node II](https://leetcode-cn.com/problems/populating-next-right-pointers-in-each-node-ii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_117.java) & Python |中等| 总结
|144|[二叉树的前序遍历](https://leetcode-cn.com/problems/binary-tree-preorder-traversal/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_144.java) & Python |中等| 树的遍历
|145|[二叉树的后序遍历](https://leetcode-cn.com/problems/binary-tree-postorder-traversal/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_145.java) & Python |困难| 树的遍历
|236|[二叉树的最近公共祖先](https://leetcode-cn.com/problems/lowest-common-ancestor-of-a-binary-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_236.java) & Python |中等| 总结
|297|[二叉树的序列化与反序列化](https://leetcode-cn.com/problems/serialize-and-deserialize-binary-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_297.java) & Python |困难| 总结



### 二叉搜索树

|  #  |      题名     |   题解    |   难度  | 标签                   
|-----|----------------|:---------------:|:--------:|:-------------:
|98|[验证二叉搜索树](https://leetcode-cn.com/problems/validate-binary-search-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search_tree/_98.java) & [Go](../master/codes/go/leetcodes/basmath/learn/binary_search_tree/98.go) |中等| Introduction to BST
|108|[将有序数组转换为二叉搜索树](https://leetcode-cn.com/problems/convert-sorted-array-to-binary-search-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search_tree/_108.java) & [Go](../master/codes/go/leetcodes/basmath/learn/binary_search_tree/108.go) |简单| Appendix: Height-balanced BST
|110|[Balanced Binary Tree](https://leetcode-cn.com/problems/balanced-binary-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search_tree/_110.java) & Python |简单| Appendix: Height-balanced BST
|173|[Binary Search Tree Iterator](https://leetcode-cn.com/problems/binary-search-tree-iterator/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search_tree/_173.java) & Python |中等| Introduction to BST
|220|[Contains Duplicate III](https://leetcode-cn.com/problems/contains-duplicate-iii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search_tree/_220.java) & Python |中等| 总结
|235|[二叉搜索树的最近公共祖先](https://leetcode-cn.com/problems/lowest-common-ancestor-of-a-binary-search-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search_tree/_235.java) & Python |简单| 总结
|450|[Delete Node in a BST](https://leetcode-cn.com/problems/delete-node-in-a-bst/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search_tree/_450.java) & Python |中等| Basic Operations in BST
|700|[Search in a Binary Search Tree](https://leetcode-cn.com/problems/search-in-a-binary-search-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search_tree/_700.java) & Python |简单| Basic Operations in BST
|701|[Insert into a Binary Search Tree](https://leetcode-cn.com/problems/insert-into-a-binary-search-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search_tree/_701.java) & Python |中等| Basic Operations in BST
|703|[Kth Largest Element in a Stream](https://leetcode-cn.com/problems/kth-largest-element-in-a-stream/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search_tree/_703.java) & Python |简单| 总结



### N叉树

|  #  |      题名     |   题解    |   难度  | 标签                   
|-----|----------------|:---------------:|:--------:|:-------------:
|429|[N-ary Tree Level Order Traversal](https://leetcode-cn.com/problems/n-ary-tree-level-order-traversal/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/n_ary_tree/_429.java) & Python |简单| Traversal
|559|[Maximum Depth of N-ary Tree](https://leetcode-cn.com/problems/maximum-depth-of-n-ary-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/n_ary_tree/_559.java) & Python |简单| Recursion
|589|[N-ary Tree Preorder Traversal](https://leetcode-cn.com/problems/n-ary-tree-preorder-traversal/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/n_ary_tree/_589.java) & Python |简单| Traversal
|590|[N-ary Tree Postorder Traversal](https://leetcode-cn.com/problems/n-ary-tree-postorder-traversal/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/n_ary_tree/_590.java) & Python |简单| Traversal



### 前缀树

|  #  |      题名     |   题解    |   难度  | 标签                   
|-----|----------------|:---------------:|:--------:|:-------------:
|208|[实现 Trie (前缀树)](https://leetcode-cn.com/problems/implement-trie-prefix-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/trie/_208.java) & Python |中等| Basic Operations
|211|[Add and Search Word - Data structure design](https://leetcode-cn.com/problems/add-and-search-word-data-structure-design/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/trie/_211.java) & Python |中等| Practical Application I
|212|[单词搜索 II](https://leetcode-cn.com/problems/word-search-ii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/trie/_212.java) & Python |困难| Practical Application II
|336|[Palindrome Pairs](https://leetcode-cn.com/problems/palindrome-pairs/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/trie/_336.java) & Python |困难| Practical Application II
|421|[Maximum XOR of Two Numbers in an Array](https://leetcode-cn.com/problems/maximum-xor-of-two-numbers-in-an-array/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/trie/_421.java) & Python |中等| Practical Application II
|648|[Replace Words](https://leetcode-cn.com/problems/replace-words/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/trie/_648.java) & Python |中等| Practical Application I
|677|[Map Sum Pairs](https://leetcode-cn.com/problems/map-sum-pairs/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/trie/_677.java) & Python |中等| Practical Application I



### 决策树

|  #  |      题名     |   题解    |   难度  | 标签                   
|-----|----------------|:---------------:|:--------:|:-------------:
| ### |[Calculate Entropy](https://leetcode-cn.com/explore/learn/card/decision-tree/285/implementation/2650/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/decision_tree/CalculateEntropy.java) & Python |简单| Implementation
| ### |[Calculate Maximum Information Gain](https://leetcode-cn.com/explore/learn/card/decision-tree/285/implementation/2651/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/decision_tree/CalculateMaxInfoGain.java) & Python |简单| Implementation



### 递归 I


|  #  |      题名     |   题解    |   难度  | 标签                   
|-----|----------------|:---------------:|:--------:|:-------------:
|21|[合并两个有序链表](https://leetcode-cn.com/problems/merge-two-sorted-lists/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_21.java) & [Go](../master/codes/go/leetcodes/basmath/learn/linked_list/21.go)|简单| 总结
|24|[Swap Nodes in Pairs](https://leetcode-cn.com/problems/swap-nodes-in-pairs/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/recursion_i/_24.java) & Python| 中等| Principle of Recursion
|50|[Pow(x, n)](https://leetcode-cn.com/problems/powx-n/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_50.java) & Python |中等| Complexity Analysis
|70|[爬楼梯](https://leetcode-cn.com/problems/climbing-stairs/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_70.java) & Python| 简单| Memoization
|95|[Unique Binary Search Trees II](https://leetcode-cn.com/problems/unique-binary-search-trees-ii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_95.java) & Python|中等| 总结
|104|[二叉树的最大深度](https://leetcode-cn.com/problems/maximum-depth-of-binary-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_104.java) & Python |简单| Complexity Analysis
|118|[帕斯卡三角形](https://leetcode-cn.com/problems/pascals-triangle/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_118.java) & Python| 简单| Recurrence Relation
|119|[帕斯卡三角形 II](https://leetcode-cn.com/problems/pascals-triangle-ii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_119.java) & Python| 简单| Recurrence Relation
|206|[反转链表](https://leetcode-cn.com/problems/reverse-linked-list/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_206.java) & Python|简单| Recurrence Relation
|344|[反转字符串](https://leetcode-cn.com/problems/reverse-string/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_344.java) & Python| 简单| Principle of Recursion
|509|[Fibonacci Number](https://leetcode-cn.com/problems/fibonacci-number/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_509.java) & Python| 简单| Memoization
|779|[K-th Symbol in Grammar](https://leetcode-cn.com/problems/k-th-symbol-in-grammar/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_779.java) & Python|中等| 总结



### 递归 II

|  #  |      题名     |   题解    |   难度  | 标签                   
|-----|----------------|:---------------:|:--------:|:-------------:
|17|[电话号码的字母组合](https://leetcode-cn.com/problems/letter-combinations-of-a-phone-number/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/recursion_ii/_17.java) & Python |中等| 总结
|22|[括号生成](https://leetcode-cn.com/problems/generate-parentheses/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/recursion_ii/_22.java) & Python |中等| Recursion to Iteration
|37|[Sudoku Solver](https://leetcode-cn.com/problems/sudoku-solver/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/recursion_ii/_37.java) & Python |困难| Backtracking
|46|[全排列](https://leetcode-cn.com/problems/permutations/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/recursion_ii/_46.java) & Python |中等| 总结
|52|[N-Queens II](https://leetcode-cn.com/problems/n-queens-ii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/recursion_ii/_52.java) & Python |困难| Backtracking
|77|[Combinations](https://leetcode-cn.com/problems/combinations/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/recursion_ii/_77.java) & Python |中等| Backtracking
|84|[柱状图中最大的矩形](https://leetcode-cn.com/problems/largest-rectangle-in-histogram/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/recursion_ii/_84.java) & Python |困难| 总结
|94|[二叉树的中序遍历](https://leetcode-cn.com/problems/binary-tree-inorder-traversal/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_94.java) & Python |中等| Recursion to Iteration
|98|[验证二叉搜索树](https://leetcode-cn.com/problems/validate-binary-search-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search_tree/_98.java) & [Go](../master/codes/go/leetcodes/basmath/learn/binary_search_tree/98.go) |中等| Divide and Conquer
|100|[Same Tree](https://leetcode-cn.com/problems/same-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/recursion_ii/_100.java) & Python |简单| Recursion to Iteration
|102|[二叉树的层次遍历](https://leetcode-cn.com/problems/binary-tree-level-order-traversal/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_102.java) & Python |中等| Recursion to Iteration
|218|[天际线问题](https://leetcode-cn.com/problems/the-skyline-problem/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/recursion_ii/_218.java) & Python |困难| 总结
|240|[搜索二维矩阵 II](https://leetcode-cn.com/problems/search-a-2d-matrix-ii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/recursion_ii/_240.java) & Python |中等| Divide and Conquer
|912|[Sort an Array](https://leetcode-cn.com/problems/sort-an-array/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/recursion_ii/_912.java) & Python|中等| Divide and Conquer



### 并发

|  #  |      题名     |   题解    |   难度  | 标签                   
|-----|----------------|:---------------:|:--------:|:-------------:
|1114|[Print in Order](https://leetcode-cn.com/problems/print-in-order/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/concurrency/_1114.java) & Python |简单| Concurrency
|1115|[Print FooBar Alternately](https://leetcode-cn.com/problems/print-foobar-alternately/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/concurrency/_1115.java) & Python |中等| Concurrency
|1116|[Print Zero Even Odd](https://leetcode-cn.com/problems/print-zero-even-odd/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/concurrency/_1116.java) & Python |中等| Concurrency
|1117|[Building H2O](https://leetcode-cn.com/problems/building-h2o/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/concurrency/_1117.java) & Python |中等| Concurrency
|1195|[Fizz Buzz Multithreaded](https://leetcode-cn.com/problems/fizz-buzz-multithreaded/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/concurrency/_1195.java) & Python |中等| Concurrency
|1226|[The Dining Philosophers](https://leetcode-cn.com/problems/the-dining-philosophers/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/concurrency/_1226.java) & Python |中等| Concurrency


### 脚本

|  #  |      题名     |   题解    |   难度  | 标签                   
|-----|----------------|:---------------:|:--------:|:-------------:
|192|[Word Frequency](https://leetcode-cn.com/problems/word-frequency/description/)|[bash](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/shell/_192.sh) |中等| Shell
|193|[Valid Phone Numbers](https://leetcode-cn.com/problems/valid-phone-numbers/description/)|[bash](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/shell/_193.sh) |简单| Shell
|194|[Transpose File](https://leetcode-cn.com/problems/transpose-file/description/)|[bash](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/shell/_194.sh) |中等| Shell
|195|[Tenth Line](https://leetcode-cn.com/problems/tenth-line/description/)|[bash](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/shell/_195.sh) |简单| Shell



### 数据库

|  #  |      题名     |   题解    |   难度  | 标签                   
|-----|----------------|:---------------:|:--------:|:-------------:
|175|[Combine Two Tables](https://leetcode-cn.com/problems/combine-two-tables/description/)|[MySQL](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/database/_175.sql) |简单| Database
|176|[第二高的薪水](https://leetcode-cn.com/problems/second-highest-salary/description/)|[MySQL](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/database/_176.sql) |简单| Database
|177|[Nth Highest Salary](https://leetcode-cn.com/problems/nth-highest-salary/description/)|[MySQL](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/database/_177.sql) |中等| Database
|178|[Rank Scores](https://leetcode-cn.com/problems/rank-scores/description/)|[MySQL](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/database/_178.sql) |中等| Database
|180|[Consecutive Numbers](https://leetcode-cn.com/problems/consecutive-numbers/description/)|[MySQL](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/database/_180.sql) |中等| Database
|181|[Employees Earning More Than Their Managers](https://leetcode-cn.com/problems/employees-earning-more-than-their-managers/description/)|[MySQL](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/database/_181.sql) |简单| Database
|182|[Duplicate Emails](https://leetcode-cn.com/problems/duplicate-emails/description/)|[MySQL](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/database/_182.sql) |简单| Database
|183|[Customers Who Never Order](https://leetcode-cn.com/problems/customers-who-never-order/description/)|[MySQL](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/database/_183.sql) |简单| Database
|184|[Department Highest Salary](https://leetcode-cn.com/problems/department-highest-salary/description/)|[MySQL](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/database/_184.sql) |中等| Database
|185|[Department Top Three Salaries](https://leetcode-cn.com/problems/department-top-three-salaries/description/)|[MySQL](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/database/_185.sql) |困难| Database
|196|[Delete Duplicate Emails](https://leetcode-cn.com/problems/delete-duplicate-emails/description/)|[MySQL](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/database/_196.sql) |简单| Database
|197|[Rising Temperature](https://leetcode-cn.com/problems/rising-temperature/description/)|[MySQL](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/database/_197.sql) |简单| Database
|262|[Trips and Users](https://leetcode-cn.com/problems/trips-and-users/description/)|[MySQL](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/database/_262.sql) |困难| Database
|595|[Big Countries](https://leetcode-cn.com/problems/big-countries/description/)|[MySQL](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/database/_595.sql) |简单| Database
|596|[Classes More Than 5 Students](https://leetcode-cn.com/problems/classes-more-than-5-students/description/)|[MySQL](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/database/_596.sql) |简单| Database
|601|[Human Traffic of Stadium](https://leetcode-cn.com/problems/human-traffic-of-stadium/description/)|[MySQL](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/database/_601.sql) |困难| Database
|620|[Not Boring Movies](https://leetcode-cn.com/problems/not-boring-movies/description/)|[MySQL](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/database/_620.sql) |简单| Database
|626|[Exchange Seats](https://leetcode-cn.com/problems/exchange-seats/description/)|[MySQL](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/database/_626.sql) |中等| Database
|627|[Swap Salary](https://leetcode-cn.com/problems/swap-salary/description/)|[MySQL](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/database/_627.sql) |简单| Database



### 排序算法

|  #  |      题名     |   题解    |   难度  | 标签                   
|-----|----------------|:---------------:|:--------:|:-------------:
|Sort 1|[冒泡排序](https://gitee.com/guobinhit/myleetcode#sort-algorithm)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basinfo/sort_algorithm/BubbleSort.java)  & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basinfo/sort_algorithm/BubbleSort.py)|简单| 排序算法
|Sort 2|[插入排序](https://gitee.com/guobinhit/myleetcode#sort-algorithm)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basinfo/sort_algorithm/InsertSort.java)  & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basinfo/sort_algorithm/InsertSort.py)|简单| 排序算法
|Sort 3|[选择排序](https://gitee.com/guobinhit/myleetcode#sort-algorithm) | [Java](../master/codes/java/leetcodes/src/main/java/com/hit/basinfo/sort_algorithm/SelectSort.java)  & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basinfo/sort_algorithm/SelectSort.py)|简单| 排序算法
|Sort 4|[快速排序](https://gitee.com/guobinhit/myleetcode#sort-algorithm)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basinfo/sort_algorithm/QuickSort.java)  & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basinfo/sort_algorithm/QuickSort.py)|简单| 排序算法
|Sort 5|[归并排序](https://gitee.com/guobinhit/myleetcode#sort-algorithm)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basinfo/sort_algorithm/MergeSort.java)  & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basinfo/sort_algorithm/MergeSort.py)|中等| 排序算法
|Sort 6|[堆排序](https://gitee.com/guobinhit/myleetcode#sort-algorithm)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basinfo/sort_algorithm/HeapSort.java)  & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basinfo/sort_algorithm/HeapSort.py)|中等| 排序算法
|Sort 7|[桶排序](https://gitee.com/guobinhit/myleetcode#sort-algorithm)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basinfo/sort_algorithm/BucketSort.java)  & Python|简单| 排序算法



### 搜索算法

|  #  |      题名     |   题解    |   难度  | 标签                   
|-----|----------------|:---------------:|:--------:|:-------------:
|Search 1|[二分搜索](https://gitee.com/guobinhit/myleetcode#search-algorithm) | [Java](../master/codes/java/leetcodes/src/main/java/com/hit/basinfo/search_algorithm/BinarySearch.java) & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basinfo/search_algorithm/BinarySearch.py)|简单| 搜索算法
|Search 2|[插值搜索](https://gitee.com/guobinhit/myleetcode#search-algorithm) | [Java](../master/codes/java/leetcodes/src/main/java/com/hit/basinfo/search_algorithm/InterpolationSearch.java) & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basinfo/search_algorithm/InterpolationSearch.py)|简单| 搜索算法
|Search 3|[顺序搜索](https://gitee.com/guobinhit/myleetcode#search-algorithm) | [Java](../master/codes/java/leetcodes/src/main/java/com/hit/basinfo/search_algorithm/OrderSearch.java) & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basinfo/search_algorithm/OrderSearch.py)|简单| 搜索算法



## 面试
### 常见面试题
#### 简单问题

|  #  |      题名     |   题解    |   难度  | 标签                   
|-----|----------------|:---------------:|:--------:|:-------------:
|1|[两数之和](https://leetcode-cn.com/problems/two-sum/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_1.java) & [Go](../master/codes/go/leetcodes/basmath/learn/hash_table/1.go)|简单| 数组
|7|[整数反转](https://leetcode-cn.com/problems/reverse-integer/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/strings/_7.java) & [Go](../master/codes/go/leetcodes/basmath/interview/top_interview_questions/easy_collection/strings/7.go) |简单| 字符串
|8|[字符串转换整数（atoi）](https://leetcode-cn.com/problems/string-to-integer-atoi/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/strings/_8.java) & Python |中等| 字符串
|13|[罗马数字转整数](https://leetcode-cn.com/problems/roman-to-integer/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/math/_13.java) & Python |简单| 数学
|14|[最长公共前缀](https://leetcode-cn.com/problems/longest-common-prefix/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_14.java) & [Go](../master/codes/go/leetcodes/basmath/learn/array_and_string/14.go)|简单| 字符串
|19|[删除链表的倒数第N个节点](https://leetcode-cn.com/problems/remove-nth-node-from-end-of-list/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_19.java) & [Go](../master/codes/go/leetcodes/basmath/learn/linked_list/19.go)|中等| 链表
|20|[有效的括号](https://leetcode-cn.com/problems/valid-parentheses/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/queue_stack/_20.java) & Python|简单| 其他
|21|[合并两个有序链表](https://leetcode-cn.com/problems/merge-two-sorted-lists/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_21.java) & [Go](../master/codes/go/leetcodes/basmath/learn/linked_list/21.go) |简单| 链表
|26|[删除排序数组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_26.java) & [Go](../master/codes/go/leetcodes/basmath/learn/array_and_string/26.go) |简单| 数组
|28|[实现 strStr()](https://leetcode-cn.com/problems/implement-strstr/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_28.java) & Python|简单| 字符串
|36|[有效的数独](https://leetcode-cn.com/problems/valid-sudoku/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_36.java) & Python |中等| 数组
|38|[外观数列](https://leetcode-cn.com/problems/count-and-say/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/strings/_38.java) & Python |简单| 字符串
|48|[旋转图像](https://leetcode-cn.com/problems/rotate-image/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/array/_48.java) & Python |中等| 数组
|53|[最大子序和](https://leetcode-cn.com/problems/maximum-subarray/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/dynamic_programming/_53.java) & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basmath/interview/top_interview_questions/easy_collection/dynamic_programming/_53.py) & [Go](../master/codes/go/leetcodes/basmath/interview/top_interview_questions/easy_collection/dynamic_programming/53.go) |简单| 动态规划
|66|[加一](https://leetcode-cn.com/problems/plus-one/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_66.java) & [Go](../master/codes/go/leetcodes/basmath/learn/array_and_string/66.go)|简单| 数组
|70|[爬楼梯](https://leetcode-cn.com/problems/climbing-stairs/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_70.java) & Python| 简单| 动态规划
|88|[合并两个有序数组](https://leetcode-cn.com/problems/merge-sorted-array/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/sorting_and_searching/_88.java) & [Go](../master/codes/go/leetcodes/basmath/interview/top_interview_questions/easy_collection/sorting_and_searching/88.go) |简单| 排序和搜索
|98|[验证二叉搜索树](https://leetcode-cn.com/problems/validate-binary-search-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search_tree/_98.java) & [Go](../master/codes/go/leetcodes/basmath/learn/binary_search_tree/98.go) |中等| 树
|101|[对称二叉树](https://leetcode-cn.com/problems/symmetric-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_101.java) & Python |简单| 树
|102|[二叉树的层次遍历](https://leetcode-cn.com/problems/binary-tree-level-order-traversal/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_102.java) & Python |中等| 树
|104|[二叉树的最大深度](https://leetcode-cn.com/problems/maximum-depth-of-binary-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_104.java) & Python |简单| 树
|108|[将有序数组转换为二叉搜索树](https://leetcode-cn.com/problems/convert-sorted-array-to-binary-search-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search_tree/_108.java) & [Go](../master/codes/go/leetcodes/basmath/learn/binary_search_tree/108.go) |简单| 树
|118|[帕斯卡三角形](https://leetcode-cn.com/problems/pascals-triangle/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_118.java) & Python| 简单| 其他
|121|[买卖股票的最佳时机](https://leetcode-cn.com/problems/best-time-to-buy-and-sell-stock/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/dynamic_programming/_121.java) & Python |简单| 动态规划
|122|[买卖股票的最佳时机 II](https://leetcode-cn.com/problems/best-time-to-buy-and-sell-stock-ii/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/array/_122.java) & Python|简单| 数组
|125|[验证回文串](https://leetcode-cn.com/problems/valid-palindrome/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/strings/_125.java) & Python |简单| 字符串
|136|[只出现一次的数字](https://leetcode-cn.com/problems/single-number/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_136.java) & Python |简单| 数组
|141|[环形链表](https://leetcode-cn.com/problems/linked-list-cycle/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_141.java) & Python|简单| 链表
|155|[最小栈](https://leetcode-cn.com/problems/min-stack/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/queue_stack/_155.java) & Python|简单| 设计问题
|189|[旋转数组](https://leetcode-cn.com/problems/rotate-array/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_189.java) & Python| 简单| 数组
|190|[颠倒二进制位](https://leetcode-cn.com/problems/reverse-bits/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/others/_190.java) & Python |简单| 其他
|191|[位1的个数](https://leetcode-cn.com/problems/number-of-1-bits/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/others/_191.java) & Python |简单| 其他
|198|[打家劫舍](https://leetcode-cn.com/problems/house-robber/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/dynamic_programming/_198.java) & Python |简单| 动态规划
|204|[计数质数](https://leetcode-cn.com/problems/count-primes/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/math/_204.java) & Python |简单| 数学
|206|[反转链表](https://leetcode-cn.com/problems/reverse-linked-list/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_206.java) & Python|简单| 链表
|217|[存在重复元素](https://leetcode-cn.com/problems/contains-duplicate/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_217.java) & Python |简单| 数组
|234|[回文链表](https://leetcode-cn.com/problems/palindrome-linked-list/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_234.java) & Python|简单| 链表
|237|[删除链表中的节点](https://leetcode-cn.com/problems/delete-node-in-a-linked-list/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/linked_list/_237.java) & Python |简单| 链表
|242|[有效的字母异位词](https://leetcode-cn.com/problems/valid-anagram/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/strings/_242.java) & Python |简单| 字符串
|268|[缺失数字](https://leetcode-cn.com/problems/missing-number/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/others/_268.java) & Python |简单| 其他
|278|[第一个错误的版本](https://leetcode-cn.com/problems/first-bad-version/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_278.java) & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basmath/learn/binary_search/_278.py)|简单| 排序和搜索
|283|[移动零](https://leetcode-cn.com/problems/move-zeroes/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_283.java) & Python| 简单| 数组
|326|[3的幂](https://leetcode-cn.com/problems/power-of-three/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/math/_326.java) & Python |简单| 数学
|344|[反转字符串](https://leetcode-cn.com/problems/reverse-string/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_344.java) & Python| 简单| 字符串
|350|[两个数组的交集 II](https://leetcode-cn.com/problems/intersection-of-two-arrays-ii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_350.java) & Python |简单| 数组
|384|[打乱数组](https://leetcode-cn.com/problems/shuffle-an-array/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/design/_384.java) & Python |中等| 设计问题
|387|[字符串中的第一个唯一字符](https://leetcode-cn.com/problems/first-unique-character-in-a-string/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_387.java) & Python |简单| 字符串
|412|[Fizz Buzz](https://leetcode-cn.com/problems/fizz-buzz/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/math/_412.java) & Python |简单| 数学
|461|[汉明距离](https://leetcode-cn.com/problems/hamming-distance/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/others/_461.java) & Python |简单| 其他



#### 中等问题

|  #  |      题名     |   题解    |   难度  | 标签                   
|-----|----------------|:---------------:|:--------:|:-------------:
|2|[两数相加](https://leetcode-cn.com/problems/add-two-numbers/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_2.java) & Python|中等| 链表
|3|[无重复字符的最长子串](https://leetcode-cn.com/problems/longest-substring-without-repeating-characters/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_3.java) & Python |中等| 数组和字符串
|5|[最长回文子串](https://leetcode-cn.com/problems/longest-palindromic-substring/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/array_and_strings/_5.java) & Python |中等| 数组和字符串
|15|[三数之和](https://leetcode-cn.com/problems/3sum/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/array_and_strings/_15.java) & Python |中等| 数组和字符串
|17|[电话号码的字母组合](https://leetcode-cn.com/problems/letter-combinations-of-a-phone-number/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/recursion_ii/_17.java) & Python |中等| 回溯算法
|22|[括号生成](https://leetcode-cn.com/problems/generate-parentheses/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search_tree/_22.java) & Python |中等| 回溯算法
|29|[两数相除](https://leetcode-cn.com/problems/divide-two-integers/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/math/_29.java) & Python |中等| 数学
|33|[搜索旋转排序数组](https://leetcode-cn.com/problems/search-in-rotated-sorted-array/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_33.java) & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basmath/learn/binary_search/_33.py)|中等| 排序和搜索
|34|[在排序数组中查找元素的第一个和最后一个位置](https://leetcode-cn.com/problems/find-first-and-last-position-of-element-in-sorted-array/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_34.java) & Python |中等| 排序和搜索
|46|[全排列](https://leetcode-cn.com/problems/permutations/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search_tree/_46.java) & Python |中等| 回溯算法
|49|[字母异位词分组](https://leetcode-cn.com/problems/group-anagrams/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_49.java) & Python |中等| 数组和字符串
|50|[Pow(x, n)](https://leetcode-cn.com/problems/powx-n/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_50.java) & Python |中等| 数学
|55|[跳跃游戏](https://leetcode-cn.com/problems/jump-game/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/dynamic_programming/_55.java) & Python |中等| 动态规划
|56|[合并区间](https://leetcode-cn.com/problems/merge-intervals/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/sorting_and_searching/_56.java) & Python |中等| 排序和搜索
|62|[不同路径](https://leetcode-cn.com/problems/unique-paths/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/dynamic_programming/_62.java) & Python |中等| 动态规划
|69|[x的平方根](https://leetcode-cn.com/problems/sqrtx/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_69.java) & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basmath/learn/binary_search/_69.py)|简单| 数学
|73|[矩阵置零](https://leetcode-cn.com/problems/set-matrix-zeroes/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/array_and_strings/_73.java) & Python |中等| 数组和字符串
|75|[颜色分类](https://leetcode-cn.com/problems/sort-colors/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/sorting_and_searching/_75.java) & Python |中等| 排序和搜索
|78|[子集](https://leetcode-cn.com/problems/subsets/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/array_and_strings/_78.java) & Python |中等| 回溯算法
|79|[单词搜索](https://leetcode-cn.com/problems/word-search/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/array_and_strings/_79.java) & Python |中等| 回溯算法
|94|[二叉树的中序遍历](https://leetcode-cn.com/problems/binary-tree-inorder-traversal/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_94.java) & Python |中等| 树和图
|103|[二叉树的锯齿形层次遍历](https://leetcode-cn.com/problems/binary-tree-zigzag-level-order-traversal/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/trees_and_graphs/_103.java) & Python |中等| 树和图
|105|[从前序与中序遍历序列构造二叉树](https://leetcode-cn.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_105.java) & Python |中等| 树和图
|116|[填充每个节点的下一个右侧节点指针](https://leetcode-cn.com/problems/populating-next-right-pointers-in-each-node/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_116.java) & Python |中等| 树和图
|150|[逆波兰表达式求值](https://leetcode-cn.com/problems/evaluate-reverse-polish-notation/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/queue_stack/_150.java) & Python|中等| 其他
|160|[相交链表](https://leetcode-cn.com/problems/intersection-of-two-linked-lists/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_160.java) & Python|简单| 链表
|162|[寻找峰值](https://leetcode-cn.com/problems/find-peak-element/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_162.java) & Python|中等| 排序和搜索
|166|[分数到小数](https://leetcode-cn.com/problems/fraction-to-recurring-decimal/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/math/_166.java) & Python |中等| 数学
|169|[多数元素](https://leetcode-cn.com/problems/majority-element/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/others/_169.java) & Python |简单| 其他
|171|[Excel表列序号](https://leetcode-cn.com/problems/excel-sheet-column-number/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/math/_171.java) & Python |简单| 数学
|172|[阶乘后的零](https://leetcode-cn.com/problems/factorial-trailing-zeroes/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/math/_172.java) & Python |简单| 数学
|200|[岛屿数量](https://leetcode-cn.com/problems/number-of-islands/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/queue_stack/_200.java) & Python|中等| 树和图
|202|[快乐数](https://leetcode-cn.com/problems/happy-number/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_202.java) & Python |简单| 数学
|215|[数组中的第K个最大元素](https://leetcode-cn.com/problems/kth-largest-element-in-an-array/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/sorting_and_searching/_215.java) & Python |中等| 排序和搜索
|230|[二叉搜索树中第K小的元素](https://leetcode-cn.com/problems/kth-smallest-element-in-a-bst/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/trees_and_graphs/_230.java) & Python |中等| 树和图
|240|[搜索二维矩阵 II](https://leetcode-cn.com/problems/search-a-2d-matrix-ii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search_tree/_240.java) & Python |中等| 排序和搜索
|297|[二叉树的序列化与反序列化](https://leetcode-cn.com/problems/serialize-and-deserialize-binary-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_297.java) & Python | 困难 | 设计问题
|300|[最长上升子序列](https://leetcode-cn.com/problems/longest-increasing-subsequence/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/dynamic_programming/_300.java) & Python |中等| 动态规划
|322|[零钱兑换](https://leetcode-cn.com/problems/coin-change/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/dynamic_programming/_322.java) & Python |中等| 动态规划
|328|[奇偶链表](https://leetcode-cn.com/problems/odd-even-linked-list/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_328.java) & Python|中等| 链表
|334|[递增的三元子序列](https://leetcode-cn.com/problems/increasing-triplet-subsequence/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/array_and_strings/_334.java) & Python |中等| 数组和字符串
|347|[前K个高频元素](https://leetcode-cn.com/problems/top-k-frequent-elements/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_347.java) & Python |中等| 排序和搜索
|371|[两整数之和](https://leetcode-cn.com/problems/sum-of-two-integers/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/others/_371.java) & Python |简单| 其他
|380|[常数时间插入、删除和获取随机元素](https://leetcode-cn.com/problems/insert-delete-getrandom-o1/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_380.java) & Python |中等| 设计问题
|621|[任务调度器](https://leetcode-cn.com/problems/task-scheduler/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/others/_621.java) & Python |中等| 其他



#### 困难问题

|  #  |      题名     |   题解    |   难度  | 标签                   
|-----|----------------|:---------------:|:--------:|:-------------:
|4|[寻找两个有序数组的中位数](https://leetcode-cn.com/problems/median-of-two-sorted-arrays/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_4.java) & Python |困难| 排序和搜索
|10|[正则表达式匹配](https://leetcode-cn.com/problems/regular-expression-matching/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/backtracking/_10.java) & Python |困难| 回溯算法
|11|[盛最多水的容器](https://leetcode-cn.com/problems/container-with-most-water/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/array_and_strings/_11.java) & Python |中等| 数组和字符串
|23|[合并K个排序链表](https://leetcode-cn.com/problems/merge-k-sorted-lists/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/linked_list/_23.java) & Python |困难| 链表
|41|[缺失的第一个正数](https://leetcode-cn.com/problems/first-missing-positive/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/array_and_strings/_41.java) & Python |困难| 数组和字符串
|42|[接雨水](https://leetcode-cn.com/problems/trapping-rain-water/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/others/_42.java) & Python |困难| 其他
|44|[通配符匹配](https://leetcode-cn.com/problems/wildcard-matching/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/backtracking/_44.java) & Python |困难| 回溯算法
|54|[螺旋矩阵](https://leetcode-cn.com/problems/spiral-matrix/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_54.java) & Python|中等| 数组和字符串
|76|[最小覆盖子串](https://leetcode-cn.com/problems/minimum-window-substring/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/array_and_strings/_76.java) & Python |困难| 数组和字符串
|84|[柱状图中最大的矩形](https://leetcode-cn.com/problems/largest-rectangle-in-histogram/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search_tree/_84.java) & Python |困难| 其他
|91|[解码方法](https://leetcode-cn.com/problems/decode-ways/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/dynamic_programming/_91.java) & Python |中等| 动态规划
|124|[二叉树中的最大路径和](https://leetcode-cn.com/problems/binary-tree-maximum-path-sum/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/trees_and_graphs/_124.java) & Python |困难| 树和图
|127|[单词接龙](https://leetcode-cn.com/problems/word-ladder/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/trees_and_graphs/_127.java) & Python |中等| 树和图
|128|[最长连续序列](https://leetcode-cn.com/problems/longest-consecutive-sequence/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/array_and_strings/_128.java) & Python |困难| 数组和字符串
|130|[被围绕的区域](https://leetcode-cn.com/problems/surrounded-regions/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/trees_and_graphs/_130.java) & Python |中等| 树和图
|131|[分割回文串](https://leetcode-cn.com/problems/palindrome-partitioning/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/backtracking/_131.java) & Python |中等| 回溯算法
|138|[复制带随机指针的链表](https://leetcode-cn.com/problems/copy-list-with-random-pointer/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_138.java) & Python|中等| 链表
|139|[单词拆分](https://leetcode-cn.com/problems/word-break/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/dynamic_programming/_139.java) & Python |中等| 动态规划
|140|[单词拆分 II](https://leetcode-cn.com/problems/word-break-ii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/dynamic_programming/_140.java) & Python |困难| 动态规划
|146|[LRU缓存机制](https://leetcode-cn.com/problems/lru-cache/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/design/_146.java) & Python |中等| 设计问题
|148|[排序链表](https://leetcode-cn.com/problems/sort-list/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/linked_list/_148.java) & Python |中等| 链表
|149|[直线上最多的点数](https://leetcode-cn.com/problems/max-points-on-a-line/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/math/_149.java) & Python |困难| 数学
|152|[乘积最大子序列](https://leetcode-cn.com/problems/maximum-product-subarray/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/dynamic_programming/_152.java) & Python |中等| 动态规划
|179|[最大数](https://leetcode-cn.com/problems/largest-number/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/math/_179.java) & Python |中等| 数学
|207|[课程表](https://leetcode-cn.com/problems/course-schedule/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/trees_and_graphs/_207.java) & Python |中等| 树和图
|208|[实现 Trie (前缀树)](https://leetcode-cn.com/problems/implement-trie-prefix-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/trie/_208.java) & Python |中等| 设计问题
|210|[课程表 II](https://leetcode-cn.com/problems/course-schedule-ii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/trees_and_graphs/_210.java) & Python |中等| 树和图
|212|[单词搜索 II](https://leetcode-cn.com/problems/word-search-ii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/trie/_212.java) & Python |困难| 回溯算法
|218|[天际线问题](https://leetcode-cn.com/problems/the-skyline-problem/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search_tree/_218.java) & Python |困难| 其他
|227|[基本计算器 II](https://leetcode-cn.com/problems/basic-calculator-ii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/array_and_strings/_227.java) & Python |中等| 数组和字符串
|236|[二叉树的最近公共祖先](https://leetcode-cn.com/problems/lowest-common-ancestor-of-a-binary-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_236.java) & Python |中等| 树和图
|238|[除自身以外数组的乘积](https://leetcode-cn.com/problems/product-of-array-except-self/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/array_and_strings/_238.java) & Python |中等| 数组和字符串
|239|[滑动窗口最大值](https://leetcode-cn.com/problems/sliding-window-maximum/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/array_and_strings/_239.java) & Python |困难| 数组和字符串
|279|[完全平方数](https://leetcode-cn.com/problems/perfect-squares/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/queue_stack/_279.java) & Python|中等| 动态规划
|287|[寻找重复数](https://leetcode-cn.com/problems/find-the-duplicate-number/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_287.java) & Python |中等| 数组和字符串
|289|[生命游戏](https://leetcode-cn.com/problems/game-of-life/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/array_and_strings/_289.java) & Python |中等| 数组和字符串
|295|[数据流的中位数](https://leetcode-cn.com/problems/find-median-from-data-stream/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/design/_295.java) & Python |困难| 设计问题
|301|[删除无效的括号](https://leetcode-cn.com/problems/remove-invalid-parentheses/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/backtracking/_301.java) & Python |困难| 回溯算法
|309|[最佳买卖股票时机含冷冻期](https://leetcode-cn.com/problems/best-time-to-buy-and-sell-stock-with-cooldown/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/dynamic_programming/_309.java) & Python |中等| 动态规划
|312|[戳气球](https://leetcode-cn.com/problems/burst-balloons/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/dynamic_programming/_312.java) & Python |困难| 动态规划
|315|[计算右侧小于当前元素的个数](https://leetcode-cn.com/problems/count-of-smaller-numbers-after-self/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/trees_and_graphs/_315.java) & Python |困难| 树和图
|324|[摆动排序 II](https://leetcode-cn.com/problems/wiggle-sort-ii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/sorting_and_searching/_324.java) & Python |中等| 排序和搜索
|329|[矩阵中的最长递增路径   ](https://leetcode-cn.com/problems/longest-increasing-path-in-a-matrix/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/trees_and_graphs/_329.java) & Python |困难| 树和图
|341|[扁平化嵌套列表迭代器](https://leetcode-cn.com/problems/flatten-nested-list-iterator/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/design/_341.java) & Python |中等| 设计问题
|378|[有序矩阵中第K小的元素](https://leetcode-cn.com/problems/kth-smallest-element-in-a-sorted-matrix/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/sorting_and_searching/_378.java) & Python |中等| 排序和搜索
|406|[根据身高重建队列](https://leetcode-cn.com/problems/queue-reconstruction-by-height/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/others/_406.java) & Python |中等| 其他
|454|[四数相加 II](https://leetcode-cn.com/problems/4sum-ii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_454.java) & Python |中等| 数组和字符串
|547|[朋友圈](https://leetcode-cn.com/problems/friend-circles/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/trees_and_graphs/_547.java) & Python |中等| 树和图



### 字节跳动

|  #  |      题名     |   题解    |   难度  | 标签                   
|-----|----------------|:---------------:|:--------:|:-------------:
|2|[两数相加](https://leetcode-cn.com/problems/add-two-numbers/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_2.java) & Python|中等| 链表与树
|3|[无重复字符的最长子串](https://leetcode-cn.com/problems/longest-substring-without-repeating-characters/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_3.java) & Python |中等| 挑战字符串
|14|[最长公共前缀](https://leetcode-cn.com/problems/longest-common-prefix/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_14.java) & [Go](../master/codes/go/leetcodes/basmath/learn/array_and_string/14.go)|简单| 挑战字符串
|15|[三数之和](https://leetcode-cn.com/problems/3sum/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/array_and_strings/_15.java) & Python |中等| 数组与排序
|21|[合并两个有序链表](https://leetcode-cn.com/problems/merge-two-sorted-lists/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_21.java) & [Go](../master/codes/go/leetcodes/basmath/learn/linked_list/21.go)|简单| 链表与树
|23|[合并K个排序链表](https://leetcode-cn.com/problems/merge-k-sorted-lists/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/linked_list/_23.java) & Python |困难| 链表与树
|33|[搜索旋转排序数组](https://leetcode-cn.com/problems/search-in-rotated-sorted-array/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_33.java) & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basmath/learn/binary_search/_33.py)|中等| 数组与排序
|42|[接雨水](https://leetcode-cn.com/problems/trapping-rain-water/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/others/_42.java) & Python |困难| 数组与排序
|43|[字符串相乘](https://leetcode-cn.com/problems/multiply-strings/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/other/_43.java) & Python |中等| 挑战字符串
|53|[最大子序和](https://leetcode-cn.com/problems/maximum-subarray/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/dynamic_programming/_53.java) & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basmath/interview/top_interview_questions/easy_collection/dynamic_programming/_53.py) & [Go](../master/codes/go/leetcodes/basmath/interview/top_interview_questions/easy_collection/dynamic_programming/53.go)|简单| 动态或贪心
|56|[合并区间](https://leetcode-cn.com/problems/merge-intervals/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/sorting_and_searching/_56.java) & Python |中等| 数组与排序
|60|[第k个排列](https://leetcode-cn.com/problems/permutation-sequence/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/other/_60.java) & Python |中等| 数组与排序
|69|[x的平方根](https://leetcode-cn.com/problems/sqrtx/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_69.java) & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basmath/learn/binary_search/_69.py)|简单| 拓展练习
|71|[简化路径](https://leetcode-cn.com/problems/simplify-path/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/other/_71.java) & Python |中等| 挑战字符串
|93|[复原IP地址](https://leetcode-cn.com/problems/restore-ip-addresses/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/other/_93.java) & Python |中等| 挑战字符串
|103|[二叉树的锯齿形层次遍历](https://leetcode-cn.com/problems/binary-tree-zigzag-level-order-traversal/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/trees_and_graphs/_103.java) & Python |中等| 链表与树
|120|[三角形最小路径和](https://leetcode-cn.com/problems/triangle/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/other/_120.java) & Python |中等| 动态或贪心
|121|[买卖股票的最佳时机](https://leetcode-cn.com/problems/best-time-to-buy-and-sell-stock/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/dynamic_programming/_121.java) & Python |简单| 动态或贪心
|122|[买卖股票的最佳时机 II](https://leetcode-cn.com/problems/best-time-to-buy-and-sell-stock-ii/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/array/_122.java) & Python|简单| 动态或贪心
|128|[最长连续序列](https://leetcode-cn.com/problems/longest-consecutive-sequence/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/array_and_strings/_128.java) & Python |困难| 数组与排序
|142|[环形链表 II](https://leetcode-cn.com/problems/linked-list-cycle-ii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_142.java) & Python|中等| 链表与树
|146|[LRU缓存机制](https://leetcode-cn.com/problems/lru-cache/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/design/_146.java) & Python |中等| 数据结构
|148|[排序链表](https://leetcode-cn.com/problems/sort-list/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/linked_list/_148.java) & Python |中等| 链表与树
|151|[翻转字符串里的单词](https://leetcode-cn.com/problems/reverse-words-in-a-string/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_151.java) & Python| 中等| 挑战字符串
|155|[最小栈](https://leetcode-cn.com/problems/min-stack/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/queue_stack/_155.java) & Python|简单| 数据结构
|160|[相交链表](https://leetcode-cn.com/problems/intersection-of-two-linked-lists/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_160.java) & Python|简单| 链表与树
|176|[第二高的薪水](https://leetcode-cn.com/problems/second-highest-salary/description/)|[MySQL](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/database/_176.sql) |简单| 拓展练习
|206|[反转链表](https://leetcode-cn.com/problems/reverse-linked-list/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_206.java) & Python|简单| 链表与树
|215|[数组中的第K个最大元素](https://leetcode-cn.com/problems/kth-largest-element-in-an-array/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/sorting_and_searching/_215.java) & Python |中等| 数组与排序
|221|[最大正方形](https://leetcode-cn.com/problems/maximal-square/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/other/_221.java) & Python |中等| 动态或贪心
|236|[二叉树的最近公共祖先](https://leetcode-cn.com/problems/lowest-common-ancestor-of-a-binary-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_236.java) & Python |中等| 链表与树
|354|[俄罗斯套娃信封问题](https://leetcode-cn.com/problems/russian-doll-envelopes/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/other/_354.java) & Python |困难| 动态或贪心
|393|[UTF-8 编码验证](https://leetcode-cn.com/problems/utf-8-validation/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/other/_393.java) & Python |中等| 拓展练习
|432|[全 O(1) 的数据结构](https://leetcode-cn.com/problems/all-oone-data-structure/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/other/_432.java) & Python |困难| 数据结构
|547|[朋友圈](https://leetcode-cn.com/problems/friend-circles/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/trees_and_graphs/_547.java) & Python |中等| 数组与排序
|567|[字符串的排列](https://leetcode-cn.com/problems/permutation-in-string/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/other/_567.java) & Python |中等| 挑战字符串
|674|[最长连续递增序列](https://leetcode-cn.com/problems/longest-continuous-increasing-subsequence/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/other/_674.java) & Python |简单| 数组与排序
|695|[岛屿的最大面积](https://leetcode-cn.com/problems/max-area-of-island/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/other/_695.java) & Python |中等| 数组与排序



### 腾讯

|  #  |      题名     |   题解    |   难度  | 标签                   
|-----|----------------|:---------------:|:--------:|:-------------:
|1|[两数之和](https://leetcode-cn.com/problems/two-sum/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_1.java) & [Go](../master/codes/go/leetcodes/basmath/learn/hash_table/1.go)  |简单| 数组与字符串
|2|[两数相加](https://leetcode-cn.com/problems/add-two-numbers/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_2.java) & Python|中等| 链表突击
|4|[寻找两个有序数组的中位数](https://leetcode-cn.com/problems/median-of-two-sorted-arrays/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_4.java) & Python |困难| 数组与字符串
|5|[最长回文子串](https://leetcode-cn.com/problems/longest-palindromic-substring/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/array_and_strings/_5.java) & Python |中等| 数组和字符串
|7|[整数反转](https://leetcode-cn.com/problems/reverse-integer/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/strings/_7.java) & [Go](../master/codes/go/leetcodes/basmath/interview/top_interview_questions/easy_collection/strings/7.go) |简单| 数学与数字
|8|[字符串转换整数（atoi）](https://leetcode-cn.com/problems/string-to-integer-atoi/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/strings/_8.java) & Python |中等| 数组与字符串
|9|[回文数](https://leetcode-cn.com/problems/palindrome-number/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/other/_9.java) & Python |简单| 数学与数字
|11|[盛最多水的容器](https://leetcode-cn.com/problems/container-with-most-water/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/array_and_strings/_11.java) & Python |中等| 数组与字符串
|14|[最长公共前缀](https://leetcode-cn.com/problems/longest-common-prefix/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_14.java) & [Go](../master/codes/go/leetcodes/basmath/learn/array_and_string/14.go)|简单| 数组与字符串
|15|[三数之和](https://leetcode-cn.com/problems/3sum/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/array_and_strings/_15.java) & Python |中等| 数组与字符串
|16|[最接近的三数之和](https://leetcode-cn.com/problems/3sum-closest/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/other/_16.java) & Python |中等| 数组与字符串
|20|[有效的括号](https://leetcode-cn.com/problems/valid-parentheses/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/queue_stack/_20.java) & Python|简单| 数组与字符串
|21|[合并两个有序链表](https://leetcode-cn.com/problems/merge-two-sorted-lists/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_21.java) & [Go](../master/codes/go/leetcodes/basmath/learn/linked_list/21.go)|简单| 链表突击
|22|[括号生成](https://leetcode-cn.com/problems/generate-parentheses/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search_tree/_22.java) & Python |中等| 回溯算法
|23|[合并K个排序链表](https://leetcode-cn.com/problems/merge-k-sorted-lists/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/linked_list/_23.java) & Python |困难| 链表突击
|26|[删除排序数组中的重复项](https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_26.java) & [Go](../master/codes/go/leetcodes/basmath/learn/array_and_string/26.go)|简单| 数组与字符串
|33|[搜索旋转排序数组](https://leetcode-cn.com/problems/search-in-rotated-sorted-array/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search/_33.java) & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basmath/learn/binary_search/_33.py)|中等| 排序与搜索
|43|[字符串相乘](https://leetcode-cn.com/problems/multiply-strings/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/other/_43.java) & Python |中等| 数组与字符串
|46|[全排列](https://leetcode-cn.com/problems/permutations/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search_tree/_46.java) & Python |中等| 回溯算法
|53|[最大子序和](https://leetcode-cn.com/problems/maximum-subarray/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/dynamic_programming/_53.java) & [Python](../master/codes/python/leetcodes/src/main/python/com/hit/basmath/interview/top_interview_questions/easy_collection/dynamic_programming/_53.py) & [Go](../master/codes/go/leetcodes/basmath/interview/top_interview_questions/easy_collection/dynamic_programming/53.go)|简单| 动态规划
|54|[螺旋矩阵](https://leetcode-cn.com/problems/spiral-matrix/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_54.java) & Python|中等| 数组和字符串
|59|[螺旋矩阵 II](https://leetcode-cn.com/problems/spiral-matrix-ii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/other/_59.java) & Python |中等| 数组和字符串
|61|[旋转链表](https://leetcode-cn.com/problems/rotate-list/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_61.java) & Python|中等| 链表突击
|62|[不同路径](https://leetcode-cn.com/problems/unique-paths/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/dynamic_programming/_62.java) & Python |中等| 动态规划
|70|[爬楼梯](https://leetcode-cn.com/problems/climbing-stairs/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_70.java) & Python| 简单| 动态规划
|78|[子集](https://leetcode-cn.com/problems/subsets/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/array_and_strings/_78.java) & Python |中等| 回溯算法
|88|[合并两个有序数组](https://leetcode-cn.com/problems/merge-sorted-array/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/sorting_and_searching/_88.java) & [Go](../master/codes/go/leetcodes/basmath/interview/top_interview_questions/easy_collection/sorting_and_searching/88.go) |简单| 数组和字符串
|89|[格雷编码](https://leetcode-cn.com/problems/gray-code/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/other/_89.java) & Python |中等| 回溯算法
|104|[二叉树的最大深度](https://leetcode-cn.com/problems/maximum-depth-of-binary-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_104.java) & Python |简单| 排序与搜索
|121|[买卖股票的最佳时机](https://leetcode-cn.com/problems/best-time-to-buy-and-sell-stock/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/dynamic_programming/_121.java) & Python |简单| 动态规划
|122|[买卖股票的最佳时机 II](https://leetcode-cn.com/problems/best-time-to-buy-and-sell-stock-ii/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/array/_122.java) & Python|简单| 动态规划
|124|[二叉树中的最大路径和](https://leetcode-cn.com/problems/binary-tree-maximum-path-sum/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/trees_and_graphs/_124.java) & Python |困难| 排序与搜索
|136|[只出现一次的数字](https://leetcode-cn.com/problems/single-number/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_136.java) & Python |简单| 数学与数字
|141|[环形链表](https://leetcode-cn.com/problems/linked-list-cycle/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_141.java) & Python|简单| 链表突击
|142|[环形链表 II](https://leetcode-cn.com/problems/linked-list-cycle-ii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_142.java) & Python|中等| 链表突击
|146|[LRU缓存机制](https://leetcode-cn.com/problems/lru-cache/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/design/_146.java) & Python |中等| 设计
|148|[排序链表](https://leetcode-cn.com/problems/sort-list/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/linked_list/_148.java) & Python |中等| 排序与搜索
|155|[最小栈](https://leetcode-cn.com/problems/min-stack/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/queue_stack/_155.java) & Python|简单| 设计
|160|[相交链表](https://leetcode-cn.com/problems/intersection-of-two-linked-lists/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_160.java) & Python|简单| 链表突击
|169|[多数元素](https://leetcode-cn.com/problems/majority-element/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/others/_169.java) & Python |简单| 数学与数字
|206|[反转链表](https://leetcode-cn.com/problems/reverse-linked-list/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_206.java) & Python|简单| 链表突击
|215|[数组中的第K个最大元素](https://leetcode-cn.com/problems/kth-largest-element-in-an-array/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/sorting_and_searching/_215.java) & Python |中等| 排序与搜索
|217|[存在重复元素](https://leetcode-cn.com/problems/contains-duplicate/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/hash_table/_217.java) & Python |简单| 数组和字符串
|230|[二叉搜索树中第K小的元素](https://leetcode-cn.com/problems/kth-smallest-element-in-a-bst/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/trees_and_graphs/_230.java) & Python |中等| 排序与搜索
|231|[2的幂](https://leetcode-cn.com/problems/power-of-two/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/other/_231.java) & Python |简单| 数学与数字
|235|[二叉搜索树的最近公共祖先](https://leetcode-cn.com/problems/lowest-common-ancestor-of-a-binary-search-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search_tree/_235.java) & Python |简单| 排序与搜索
|236|[二叉树的最近公共祖先](https://leetcode-cn.com/problems/lowest-common-ancestor-of-a-binary-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_tree/_236.java) & Python |中等| 排序与搜索
|237|[删除链表中的节点](https://leetcode-cn.com/problems/delete-node-in-a-linked-list/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/easy_collection/linked_list/_237.java) & Python |简单| 链表突击
|238|[除自身以外数组的乘积](https://leetcode-cn.com/problems/product-of-array-except-self/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/hard_collection/array_and_strings/_238.java) & Python |中等| 数组和字符串
|292|[Nim 游戏](https://leetcode-cn.com/problems/nim-game/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/other/_292.java) & Python |简单| 附加
|344|[反转字符串](https://leetcode-cn.com/problems/reverse-string/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_344.java) & Python| 简单| 数组和字符串
|557|[反转字符串中的单词 III](https://leetcode-cn.com/problems/reverse-words-in-a-string-iii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/array_and_string/_557.java) & Python |简单| 数组和字符串


### 猿题库

|  #  |      题名     |   题解    |   难度  | 标签                   
|-----|----------------|:---------------:|:--------:|:-------------:
|369|[单链表加1](https://leetcode-cn.com/problems/plus-one-linked-list/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/other/_369.java) & Python |中等| 链表




### 搜狗

|  #  |      题名     |   题解    |   难度  | 标签                   
|-----|----------------|:---------------:|:--------:|:-------------:
|168|[Excel表列名称](https://leetcode-cn.com/problems/excel-sheet-column-title/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/other/_168.java) & Python |简单| 数学
|171|[Excel表列序号](https://leetcode-cn.com/problems/excel-sheet-column-number/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/interview/top_interview_questions/medium_collection/math/_171.java) & Python |简单| 数学


### 美团点评

|  #  |      题名     |   题解    |   难度  | 标签                   
|-----|----------------|:---------------:|:--------:|:-------------:
|2|[两数相加](https://leetcode-cn.com/problems/add-two-numbers/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/linked_list/_2.java) & Python|中等| 链表
|43|[字符串相乘](https://leetcode-cn.com/problems/multiply-strings/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/other/_43.java) & Python |中等| 字符串
|74|[搜索二维矩阵](https://leetcode-cn.com/problems/search-a-2d-matrix/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/other/_74.java) & Python |中等| 数组
|108|[将有序数组转换为二叉搜索树](https://leetcode-cn.com/problems/convert-sorted-array-to-binary-search-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/binary_search_tree/_108.java) & [Go](../master/codes/go/leetcodes/basmath/learn/binary_search_tree/108.go) |简单| 树
|109|[有序链表转换二叉搜索树](https://leetcode-cn.com/problems/convert-sorted-list-to-binary-search-tree/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/other/_109.java) & Python |中等| 树
|240|[搜索二维矩阵 II](https://leetcode-cn.com/problems/search-a-2d-matrix-ii/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/recursion_ii/_240.java) & Python |中等| 数组
|415|[字符串相加](https://leetcode-cn.com/problems/add-strings/description/)|[Java](../master/codes/java/leetcodes/src/main/java/com/hit/basmath/learn/other/_415.java) & Python |简单| 字符串




------------


- [↑↑↑ 返回顶部 ↑↑↑](#索引)




