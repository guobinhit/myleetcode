# myleetcode

![author](https://img.shields.io/badge/author-chariesgavin-blueviolet.svg)![language](https://img.shields.io/badge/language-Go-blue.svg)![license](https://img.shields.io/badge/license-MIT-brightgreen.svg)

## 索引

- [常见问题](#常见问题)


## 常见问题
**1. 项目内实体及工具方法引用失败？**

解：执行`go mod init`初始化命令，开启`Go modules integration`。
