package common

// GetMax 获取最大值
func GetMax(a, b int) int {
	if a >= b {
		return a
	}
	return b
}

// GetMin 获取最小值
func GetMin(a, b int) int {
	if a >= b {
		return b
	}
	return a
}
