package common

// ListNode 单链表节点
type ListNode struct {
	Val  int
	Next *ListNode
}

// TreeNode 树节点
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}
