package binary_tree

import "leetcodes/common"

/**
101. 对称二叉树

给定一个二叉树，检查它是否是镜像对称的。

例如，二叉树 [1,2,2,3,4,4,3] 是对称的。
    1
   / \
  2   2
 / \ / \
3  4 4  3

但是下面这个[1,2,2,null,3,null,3] 则不是镜像对称的:
    1
   / \
  2   2
   \   \
   3    3

进阶：你可以运用递归和迭代两种方法解决这个问题吗？
*/

func isSymmetric(root *common.TreeNode) bool {
	return root == nil || helper(root.Left, root.Right)
}

// 递归方法
func helper(left, right *common.TreeNode) bool {
	if left == nil || right == nil {
		return left == right
	}
	if left.Val == right.Val && helper(left.Left, right.Right) && helper(left.Right, right.Left) {
		return true
	}
	return false
}
