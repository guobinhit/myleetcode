package hash_table

/**
1. 两数之和

给定一个整数数组 nums 和一个整数目标值 target，请你在该数组中找出 和为目标值 target  的那 两个 整数，并返回它们的数组下标。
你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。
你可以按任意顺序返回答案。

示例 1：
输入：nums = [2,7,11,15], target = 9
输出：[0,1]
解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。

示例 2：
输入：nums = [3,2,4], target = 6
输出：[1,2]

示例 3：
输入：nums = [3,3], target = 6
输出：[0,1]

提示：
2 <= nums.length <= 104
-109 <= nums[i] <= 109
-109 <= target <= 109
只会存在一个有效答案

进阶：你可以想出一个时间复杂度小于 O(n2) 的算法吗？
*/

func twoSum(nums []int, target int) []int {
	// map[数组中元素]数组中元素索引
	hashTable := map[int]int{}
	// 依次循环数组中的每一个元素
	for i, x := range nums {
		// 利用target-x的值，到map中进行检索
		// 如果检索到了，则说明map中存在一个k，与当前遍历的元素之和等于目标数值
		// 进而，将k的值(切片中元素的索引)，与当前遍历元素的索引，放到一个切片中返回即可
		if p, ok := hashTable[target-x]; ok {
			return []int{p, i}
		}

		// 如果没有检索到，则将当前遍历的元素与索引值存到map之中
		hashTable[x] = i
	}
	return nil
}
