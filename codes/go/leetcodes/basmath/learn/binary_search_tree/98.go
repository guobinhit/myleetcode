package binary_search_tree

import (
	"leetcodes/common"
	"math"
)

/**
98. 验证二叉搜索树

给你一个二叉树的根节点 root ，判断其是否是一个有效的二叉搜索树。

有效 二叉搜索树定义如下：
节点的左子树只包含 小于 当前节点的数。
节点的右子树只包含 大于 当前节点的数。
所有左子树和右子树自身必须也是二叉搜索树。

示例 1：
输入：root = [2,1,3]
输出：true

示例 2：
输入：root = [5,1,4,null,null,3,6]
输出：false
解释：根节点的值是 5 ，但是右子节点的值是 4 。

提示：
树中节点数目范围在[1, 10^4] 内
-2^31 <= Node.val <= 2^31 - 1
*/

func isValidBST(root *common.TreeNode) bool {
	return helper(root, math.MinInt64, math.MaxInt64)
}

// 递归方法
func helper(root *common.TreeNode, lower, upper int) bool {
	// 如果root为nil，直接返回true即可
	if root == nil {
		return true
	}
	// 如果当前节点的值小于等于最小值或者大于等于最大值，都属于不满足的条件
	if root.Val <= lower || root.Val >= upper {
		return false
	}
	// 仅当左右子树都满足二叉搜索树的条件的时候，当前树才是二叉搜索树
	return helper(root.Left, lower, root.Val) && helper(root.Right, root.Val, upper)
}
