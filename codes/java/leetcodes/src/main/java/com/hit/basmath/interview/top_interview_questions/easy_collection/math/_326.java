package com.hit.basmath.interview.top_interview_questions.easy_collection.math;

/**
 * 326. 3的幂
 * <p>
 * 给定一个整数，写一个函数来判断它是否是 3 的幂次方。
 * <p>
 * 示例 1:
 * <p>
 * 输入: 27
 * 输出: true
 * <p>
 * 示例 2:
 * <p>
 * 输入: 0
 * 输出: false
 * <p>
 * 示例 3:
 * <p>
 * 输入: 9
 * 输出: true
 * <p>
 * 示例 4:
 * <p>
 * 输入: 45
 * 输出: false
 * <p>
 * 进阶：
 * 你能不使用循环或者递归来完成本题吗？
 */
public class _326 {
    public boolean isPowerOfThree(int n) {
        // 1162261467 is 3^19,  3^20 is bigger than int
//        return (n > 0 && Math.pow(3, 19) % n == 0);
        return (n > 0 && 1162261467 % n == 0);
    }

    public boolean isPowerOfThree2(int n) {
        return (Math.log10(n) / Math.log10(3)) % 1 == 0;
    }

    public boolean isPowerOfThree3(int n) {
        if (n < 1) {
            return false;
        }
        while (n % 3 == 0) {
            n /= 3;
        }
        return n == 1;
    }
}
