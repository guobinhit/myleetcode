package com.hit.basmath.interview.top_interview_questions.medium_collection.design;

import java.util.*;

/**
 * 380. 常数时间插入、删除和获取随机元素
 * <p>
 * 设计一个支持在平均 时间复杂度 O(1) 下，执行以下操作的数据结构。
 * <p>
 * 1. insert(val)：当元素 val 不存在时，向集合中插入该项。
 * 2. remove(val)：元素 val 存在时，从集合中移除该项。
 * 3. getRandom：随机返回现有集合中的一项。每个元素应该有相同的概率被返回。
 * <p>
 * 示例 :
 * <p>
 * // 初始化一个空的集合。
 * RandomizedSet randomSet = new RandomizedSet();
 * <p>
 * // 向集合中插入 1 。返回 true 表示 1 被成功地插入。
 * randomSet.insert(1);
 * <p>
 * // 返回 false ，表示集合中不存在 2 。
 * randomSet.remove(2);
 * <p>
 * // 向集合中插入 2 。返回 true 。集合现在包含 [1,2] 。
 * randomSet.insert(2);
 * <p>
 * // getRandom 应随机返回 1 或 2 。
 * randomSet.getRandom();
 * <p>
 * // 从集合中移除 1 ，返回 true 。集合现在包含 [2] 。
 * randomSet.remove(1);
 * <p>
 * // 2 已在集合中，所以返回 false 。
 * randomSet.insert(2);
 * <p>
 * // 由于 2 是集合中唯一的数字，getRandom 总是返回 2 。
 * randomSet.getRandom();
 */
public class _380 {
    class RandomizedSet {
        private Map<Integer, Integer> dict;
        private List<Integer> list;
        private Random rand = new Random();

        public RandomizedSet() {
            dict = new HashMap<>();
            list = new ArrayList<>();
        }

        public boolean insert(int val) {
            if (dict.containsKey(val)) {
                return false;
            }
            dict.put(val, list.size());
            list.add(list.size(), val);
            return true;
        }

        public boolean remove(int val) {
            if (!dict.containsKey(val)) {
                return false;
            }
            int lastElement = list.get(list.size() - 1);
            int idx = dict.get(val);
            list.set(idx, lastElement);
            dict.put(lastElement, idx);
            list.remove(list.size() - 1);
            dict.remove(val);
            return true;
        }

        public int getRandom() {
            return list.get(rand.nextInt(list.size()));
        }
    }
}
