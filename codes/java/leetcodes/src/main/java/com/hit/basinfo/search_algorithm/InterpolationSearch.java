package com.hit.basinfo.search_algorithm;

/**
 * author:Charies Gavin
 * date:2019/11/8,16:28
 * https:github.com/guobinhit
 * description: 插值搜索
 */
public class InterpolationSearch {
    /**
     * 插值搜索
     *
     * @param nums   待搜索数组
     * @param left   左侧索引，初始值为 0
     * @param right  右侧索引，初始值为 nums.length -1
     * @param target 目标元素
     * @return 目标元素的索引
     */
    public int interpolationSearch(int[] nums, int left, int right, int target) {
        if (left > right || target < nums[left] || target > nums[right]) {
            return -1;
        }

        int mid = left + (right - left) * (target - nums[left]) / (nums[right] - nums[left]);

        if (target > nums[mid]) {
            return interpolationSearch(nums, mid + 1, right, target);
        } else if (target < nums[mid]) {
            return interpolationSearch(nums, left, mid - 1, target);
        } else {
            return mid;
        }
    }
}
