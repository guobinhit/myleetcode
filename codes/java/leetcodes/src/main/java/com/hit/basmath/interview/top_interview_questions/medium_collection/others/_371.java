package com.hit.basmath.interview.top_interview_questions.medium_collection.others;

/**
 * 371. 两整数之和
 * <p>
 * 不使用运算符 + 和 - ​​​​​​​，计算两整数 ​​​​​​​a 、b ​​​​​​​之和。
 * <p>
 * 示例 1:
 * <p>
 * 输入: a = 1, b = 2
 * 输出: 3
 * <p>
 * 示例 2:
 * <p>
 * 输入: a = -2, b = 3
 * 输出: 1
 */
public class _371 {
    public int getSum(int a, int b) {
        if (a == 0) return b;
        if (b == 0) return a;
        // 进位为 0 时，即 a 为最终的求和结果
        while (b != 0) {
            // 二进制每位相加就相当于各位做异或操作
            int carry = a ^ b;
            // 求进位值相当于二进制各位进行与操作，再左移一位
            b = (a & b) << 1;
            a = carry;
        }
        return a;
    }
}
