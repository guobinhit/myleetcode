package com.hit.basmath.interview.we_meet.bytedance;

/**
 * 14. 最长公共前缀
 * <p>
 * 编写一个函数来查找字符串数组中的最长公共前缀。
 * <p>
 * 如果不存在公共前缀，返回空字符串 ""。
 * <p>
 * 示例 1:
 * <p>
 * 输入: ["flower","flow","flight"]
 * 输出: "fl"
 * <p>
 * 示例 2:
 * <p>
 * 输入: ["dog","racecar","car"]
 * 输出: ""
 * 解释: 输入不存在公共前缀。
 * <p>
 * 说明: 所有输入只包含小写字母 a-z 。
 */
public class _14 {
    public String longestCommonPrefix(String[] strs) {
        if (strs == null || strs.length == 0) return "";
        String aimPrefix = strs[0];
        for (String s : strs) {
            while (s.indexOf(aimPrefix) != 0) {
                aimPrefix = aimPrefix.substring(0, aimPrefix.length() - 1);
            }
            if (aimPrefix.length() == 0) {
                return "";
            }
        }
        return aimPrefix;
    }
}
