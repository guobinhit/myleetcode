package com.hit.basinfo.sort_algorithm;

/**
 * author:Charies Gavin
 * date:2019/1/7,14:20
 * https:github.com/guobinhit
 * description: 冒泡排序
 */
public class BubbleSort {
    /**
     * 冒泡排序
     *
     * @param nums 待排序数组
     */
    public void bubbleSort(int[] nums) {
        if (nums != null && nums.length > 1) {
            for (int i = nums.length - 1; i > 0; i--) {
                for (int j = 0; j < i; j++) {
                    if (nums[j] > nums[j + 1]) {
                        int temp = nums[j];
                        nums[j] = nums[j + 1];
                        nums[j + 1] = temp;
                    }
                }
            }
        }
    }
}
