package com.hit.basmath.interview.top_interview_questions.medium_collection.array_and_strings;

/**
 * 5. 最长回文子串
 * <p>
 * 给定一个字符串 s，找到 s 中最长的回文子串。你可以假设 s 的最大长度为 1000。
 * <p>
 * 示例 1：
 * <p>
 * 输入: "babad"
 * 输出: "bab"
 * 注意: "aba" 也是一个有效答案。
 * <p>
 * 示例 2：
 * <p>
 * 输入: "cbbd"
 * 输出: "bb"
 */
public class _5 {
    public String longestPalindrome(String s) {
        int n = s.length();
        boolean[][] dp = new boolean[n][n];
        String ans = "";
        for (int index = 0; index < n; index++) {
            for (int i = 0; i + index < n; i++) {
                int j = i + index;
                if (index == 0) {
                    dp[i][j] = true;
                } else if (index == 1) {
                    dp[i][j] = (s.charAt(i) == s.charAt(j));
                } else {
                    dp[i][j] = (s.charAt(i) == s.charAt(j) && dp[i + 1][j - 1]);
                }
                if (dp[i][j] && index + 1 > ans.length()) {
                    ans = s.substring(i, i + index + 1);
                }
            }
        }
        return ans;
    }
}
