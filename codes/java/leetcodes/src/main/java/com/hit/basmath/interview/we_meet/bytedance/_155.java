package com.hit.basmath.interview.we_meet.bytedance;

import com.hit.common.ListNode;

import java.util.Stack;

/**
 * 155. 最小栈
 * <p>
 * 设计一个支持 push ，pop ，top 操作，并能在常数时间内检索到最小元素的栈。
 * <p>
 * push(x) —— 将元素 x 推入栈中。
 * pop() —— 删除栈顶的元素。
 * top() —— 获取栈顶元素。
 * getMin() —— 检索栈中的最小元素。
 * <p>
 * 示例:
 * <p>
 * 输入：
 * ["MinStack","push","push","push","getMin","pop","top","getMin"]
 * [[],[-2],[0],[-3],[],[],[],[]]
 * <p>
 * 输出：
 * [null,null,null,null,-3,null,0,-2]
 * <p>
 * 解释：
 * MinStack minStack = new MinStack();
 * minStack.push(-2);
 * minStack.push(0);
 * minStack.push(-3);
 * minStack.getMin();   --> 返回 -3.
 * minStack.pop();
 * minStack.top();      --> 返回 0.
 * minStack.getMin();   --> 返回 -2.
 *  
 * 提示：
 * <p>
 * pop、top 和 getMin 操作总是在 非空栈 上调用。
 */
public class _155 {
    class MinStack {
        private int min = Integer.MAX_VALUE;
        private Stack<Integer> stack = new Stack<Integer>();

        public void push(int x) {
            if (x <= min) {
                stack.push(min);
                min = x;
            }
            stack.push(x);
        }

        public void pop() {
            if (stack.pop() == min) {
                min = stack.pop();
            }
        }

        public int top() {
            return stack.peek();
        }

        public int getMin() {
            return min;
        }
    }

    class MinStack2 {
        private int minValue;
        private ListNode head = null;

        /**
         * initialize your data structure here.
         */
        public MinStack2() {
            head = new ListNode(-1);
        }

        public void push(int x) {
            ListNode value = new ListNode(x);
            ListNode temp = head.next;

            if (head.next == null) {
                minValue = x;
            } else {
                if (x < minValue) {
                    minValue = x;
                }
            }

            head.next = value;
            value.next = temp;
        }

        public void pop() {
            if (head.next != null) {
                int currValue = head.next.val;
                head.next = head.next.next;
                if (currValue == minValue) {
                    minValue = this.throughMin();
                }
            }
        }

        public int top() {
            if (head.next != null) {
                return head.next.val;
            }
            return -1;
        }

        public int getMin() {
            if (head.next != null) {
                return minValue;
            }
            return -1;
        }

        private int throughMin() {
            int min = -1;
            ListNode curr = head.next;
            if (curr != null) {
                min = curr.val;

                while (curr != null) {
                    if (curr.val < min) {
                        min = curr.val;
                    }
                    curr = curr.next;
                }
            }
            return min;
        }
    }
}
