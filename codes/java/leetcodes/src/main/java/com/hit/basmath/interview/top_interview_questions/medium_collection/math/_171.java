package com.hit.basmath.interview.top_interview_questions.medium_collection.math;

/**
 * 171. Excel表列序号
 * <p>
 * 给定一个Excel表格中的列名称，返回其相应的列序号。
 * <p>
 * 例如，
 * <p>
 * A -> 1
 * B -> 2
 * C -> 3
 * ...
 * Z -> 26
 * AA -> 27
 * AB -> 28
 * ...
 * <p>
 * 示例 1:
 * <p>
 * 输入: "A"
 * 输出: 1
 * <p>
 * 示例 2:
 * <p>
 * 输入: "AB"
 * 输出: 28
 * <p>
 * 示例 3:
 * <p>
 * 输入: "ZY"
 * 输出: 701
 */
public class _171 {
    public int titleToNumber(String s) {
        if (s == null || "".equals(s)) {
            return -1;
        }
        // 将字符串转换为字符数组
        char[] strArr = s.toCharArray();
        // 指数
        int exp = 0;
        // 行数
        int col = 0;
        for (int i = strArr.length - 1; i >= 0; i--) {
            col += (strArr[i] - 'A' + 1) * Math.pow(26, exp);
            exp++;
        }
        return col;
    }
}
