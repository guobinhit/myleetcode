package com.hit.basinfo.search_algorithm;

/**
 * author:Charies Gavin
 * date:2019/1/7,15:00
 * https:github.com/guobinhit
 * description: 顺序搜索
 */
public class OrderSearch {
    /**
     * 顺序搜索
     *
     * @param nums   待搜索数组
     * @param target 目标元素
     * @return 目标元素的索引
     */
    public int orderSearch(int[] nums, int target) {
        if (nums == null) {
            return -1;
        }
        if (nums.length < 2 && nums[0] != target) {
            return -1;
        }

        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] == target) {
                return i;
            }
        }
        return -1;
    }
}
