package com.hit.basinfo.sort_algorithm;

/**
 * author:Charies Gavin
 * date:2019/1/7,15:16
 * https:github.com/guobinhit
 * description: 选择排序
 */
public class SelectSort {
    /**
     * 选择排序
     *
     * @param nums 待排序数组
     */
    public void selectSort(int[] nums) {
        if (nums != null && nums.length > 1) {
            for (int i = nums.length - 1; i > 0; i--) {
                int maxIndex = i;

                for (int j = 0; j < i; j++) {
                    if (nums[maxIndex] < nums[j]) {
                        maxIndex = j;
                    }
                }

                if (maxIndex != i) {
                    int temp = nums[i];
                    nums[i] = nums[maxIndex];
                    nums[maxIndex] = temp;
                }
            }
        }
    }
}
