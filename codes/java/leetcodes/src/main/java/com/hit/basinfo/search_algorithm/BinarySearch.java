package com.hit.basinfo.search_algorithm;

/**
 * author:Charies Gavin
 * date:2019/1/7,14:31
 * https:github.com/guobinhit
 * description: 二分搜索
 */
public class BinarySearch {
    /**
     * 二分搜索
     * <p>
     * 待搜索数组，必须是有序的
     *
     * @param nums   待搜索数组
     * @param target 目标元素
     * @return 目标元素的索引
     */
    public int binarySearch(int[] nums, int target) {
        if (nums == null) {
            return -1;
        }

        if (nums.length < 2 && nums[0] != target) {
            return -1;
        }

        int left = 0, right = nums.length - 1;

        while (left <= right) {
            int mid = (left + right) / 2;
            if (nums[mid] == target) {
                return mid;
            } else if (nums[mid] < target) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        return -1;
    }
}
