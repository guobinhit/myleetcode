package com.hit.basmath.interview.top_interview_questions.medium_collection.others;

/**
 * 169. 多数元素
 * <p>
 * 给定一个大小为 n 的数组，找到其中的多数元素。多数元素是指在数组中出现次数大于 ⌊ n/2 ⌋ 的元素。
 * <p>
 * 你可以假设数组是非空的，并且给定的数组总是存在多数元素。
 * <p>
 * 示例 1:
 * <p>
 * 输入: [3,2,3]
 * 输出: 3
 * <p>
 * 示例 2:
 * <p>
 * 输入: [2,2,1,1,1,2,2]
 * 输出: 2
 */
public class _169 {
    public int majorityElement(int[] nums) {
        int count = 0, ans = 0;
        for (int num : nums) {
            if (count == 0) {
                ans = num;
            }
            if (num != ans) {
                count--;
            } else {
                count++;
            }
        }
        return ans;
    }
}
