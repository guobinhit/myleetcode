package com.hit.basmath.interview.top_interview_questions.easy_collection.linked_list;

import com.hit.common.ListNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 234. 回文链表
 * <p>
 * 请判断一个链表是否为回文链表。
 * <p>
 * 示例 1:
 * <p>
 * 输入: 1->2
 * 输出: false
 * <p>
 * 示例 2:
 * <p>
 * 输入: 1->2->2->1
 * 输出: true
 * <p>
 * 进阶：你能否用 O(n) 时间复杂度和 O(1) 空间复杂度解决此题？
 */
public class _234 {
    public boolean isPalindrome(ListNode head) {
        List<Integer> ans = new ArrayList<>();
        while (head != null) {
            ans.add(head.val);
            head = head.next;
        }
        int front = 0, back = ans.size() - 1;
        while (front < back) {
            if (!Objects.equals(ans.get(front), ans.get(back))) {
                return false;
            }
            front++;
            back--;
        }
        return true;
    }
}
