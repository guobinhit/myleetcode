package com.hit.basmath.learn.other;

/**
 * 168. Excel表列名称
 * <p>
 * 给定一个正整数，返回它在 Excel 表中相对应的列名称。
 * <p>
 * 例如，
 * <p>
 * 1 -> A
 * 2 -> B
 * 3 -> C
 * ...
 * 26 -> Z
 * 27 -> AA
 * 28 -> AB
 * ...
 * <p>
 * 示例 1:
 * <p>
 * 输入: 1
 * 输出: "A"
 * <p>
 * 示例 2:
 * <p>
 * 输入: 28
 * 输出: "AB"
 * <p>
 * 示例 3:
 * <p>
 * 输入: 701
 * 输出: "ZY"
 */
public class _168 {
    public String convertToTitle(int n) {
        StringBuilder sb = new StringBuilder();
        while (n != 0) {
            int temp = n % 26;
            n /= 26;
            if (temp == 0) {
                temp = 26;
                n = n - 1;
            }
            sb.append((char) (temp + 'A' - 1));
        }
        return sb.reverse().toString();
    }
}
